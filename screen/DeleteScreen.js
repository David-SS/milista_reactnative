import React from 'react';
import stylesheet, { currentTheme } from '../Style'
import { AsyncStorage, View, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Badge, SearchBar, Icon, CheckBox } from 'react-native-elements';

class DeleteScreen extends React.Component {
    static navigationOptions = ({ navigation })=>{
        const { navigate } = navigation;
        const styles = stylesheet();
        return{
            title: 'Eliminar',
            headerLeft: (
                <View style={styles.headerButton}>
                    <Icon
                    name='arrow-back'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => navigation.navigate('Home')}
                    />
                </View>
            ),
            headerRight: (
                <View style={styles.headerButton}>
                    <Icon
                    name='menu'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => navigation.openDrawer()}
                    />
                </View>
            )
        } 
    };

    constructor(props) {
        super(props);
        this.state={
          searchText: '',
          todoList: [],
          todoListSelection: []
        }      
      }
  
      componentWillMount() {
        this.navigationEvents = [
          this.props.navigation.addListener('didFocus', this.refreshTodoList)
        ];
      }
  
      componentWillUnmount() {
        this.navigationEvents.forEach(sub => sub.remove());
      }
       
      render() {
        const {navigate} = this.props.navigation;
        const styles = stylesheet();
        return (
          <View style={styles.container}> 
            <View style={styles.searchBarContainer}>
                <View style={styles.searchBar}>
                    <SearchBar
                        inputStyle={[styles.searchBarColor, {color: currentTheme.text}]}
                        containerStyle={[styles.searchBarColor]}
                        placeholderTextColor={currentTheme.text}
                        onChangeText={ (searchTextValue) => {this.setState(() => ({ searchText : searchTextValue }))} }
                        placeholder='Escribe el nombre de tu nota...' 
                        value={ this.state.searchText }
                    />
                </View>
                <View style={styles.badge}>
                    <Badge
                        value={this.state.todoList.length}
                    />
                </View>
            </View>
            <View style={styles.todoList}>
              <ScrollView>
                {
                  this.state.todoList.map((todo) => (this.filterTodos(todo)))
                }
              </ScrollView>
            </View>
            <View>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {this.deleteTodos()}}
                  style={styles.touchableOpacityStyle}>
                  <Image
                    source={require('../assets/Icons/delete.png')}
                    style={styles.floatingButtonStyle}
                  />
                </TouchableOpacity>
            </View>     
        </View>
        ); 
    }
  
    refreshTodoList = () => {
      AsyncStorage.getItem('todos').then((todos) => {
        if (todos !== null) {
          todos = JSON.parse(todos);
          this.setState({todoList: todos});
        }
      }); 
    }
  
    filterTodos = (todo) => {
      if (todo.title.toString().toLowerCase().indexOf(this.state.searchText.toString().toLowerCase()) > -1) {
        return (
            <CheckBox 
                key={todo.id}
                title={todo.title}
                textStyle={{ color: currentTheme.text, fontWeight: 'bold' }}
                containerStyle={{backgroundColor: currentTheme.third, borderBottomColor: currentTheme.text}}
                checkedColor={currentTheme.text}
                uncheckedColor={currentTheme.text}
                checked={this.isOnCheckTodoList(todo)}
                onPress={() => this.selectTodoListItem(todo)}
            />
        )
      }
    }

    isOnCheckTodoList(todo) {
        let localSelectTodoListItem = this.state.todoListSelection;
        let index = localSelectTodoListItem.findIndex(todoId => todoId.id === todo.id);
        if (index === -1) {
            return false;
        } else {
            return true;
        }
    }

    selectTodoListItem(todo) {
        let localSelectTodoListItem = this.state.todoListSelection;
        let index = localSelectTodoListItem.findIndex(todoId => todoId.id === todo.id);     
        if (index === -1) {
            localSelectTodoListItem.push({id: todo.id, title: todo.title, description: todo.description});
            this.setState({todoListSelection: localSelectTodoListItem});
            return true;
        } else {
            localSelectTodoListItem.splice(index,1);
            this.setState({todoListSelection: localSelectTodoListItem});
            return false;
        }
    }

    deleteTodos() {
        if (this.state.todoListSelection.length === 0) {
            Alert.alert(
                '¿Eliminar toda la lista?',
                'Se va a eliminar toda la lista de tareas, ¿esta seguro?',
                [
                  {text: 'Eliminar', onPress: () => {
                      this.setState({todoList: []});
                      AsyncStorage.multiRemove(['todos']);
                  }},
                  {text: 'Cancelar'}
                ],
                { cancelable: false }
            )
        } else {
            let todos = this.state.todoList;
            this.state.todoListSelection.forEach((value) => {
                let index = todos.findIndex(todo=> todo.id === value.id);     
                todos.splice(index,1);
            })
            this.setState({todoListSelection: []});
            AsyncStorage.setItem('todos', JSON.stringify(todos));
        }
    }
}
export default DeleteScreen;