import React from 'react';
import stylesheet, { currentTheme } from '../Style'
import { AsyncStorage, View, TextInput, ScrollView } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { StackActions, NavigationActions } from 'react-navigation';

class EditScreen extends React.Component {

    static navigationOptions = ({ navigation })=>{
        const { params = {} } = navigation.state;
        const { navigate } = navigation;
        const styles = stylesheet();
        return{
            title: 'Editar nota',
            headerLeft: (
                <View style={styles.headerButton}>
                    <Icon
                    name='arrow-back'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => params._this.goHomeAction()}
                    />
                </View>
            ),
            headerRight: (
                <View style={styles.headerButton}>
                    <Icon
                    name='save'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => params._this.saveNote()}
                    />
                </View>
            )
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            title: '',
            description: ''
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ _this: this })
        this.setState({
            id: this.props.navigation.getParam('id'),
            title: this.props.navigation.getParam('title'), 
            description: this.props.navigation.getParam('description')
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        const styles = stylesheet();
        return (
            <View style={styles.container}>
                <View style={styles.editContainer}>
                    <Text h3 style={{color: currentTheme.text}}>Título</Text>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={(title) => this.setState({title: title})}
                        placeholder="Escribe tu título aquí..."
                        placeholderTextColor={currentTheme.text}
                        value={this.state.title}
                    />
                    <Text h3 style={{color: currentTheme.text}}>Descripción</Text>
                    <ScrollView>
                        <TextInput
                        style={styles.textInput}
                        tintColor={"black"}
                        multiline={true}
                        numberOfLines={4}
                        onChangeText={(description) => this.setState({description: description})}
                        placeholder="Escribe tu descripción aquí..."
                        placeholderTextColor={currentTheme.text}
                        value={this.state.description}
                        />
                    </ScrollView>
                </View>
            </View>
        );
  }

  goHomeAction = () => {
    const resetAction = StackActions.reset({
        key: null,
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  saveNote = async() => {
    try {
        AsyncStorage.getItem('todos').then((todos) => {
            if (todos === null) {
                let newTodos = [];
                newTodos.push({id: new Date(), title: this.state.title, description: this.state.description})
                AsyncStorage.setItem('todos', JSON.stringify(newTodos));
                console.log('ADD FIRST TODO');
                console.log(newTodos);
            } else {
                todos = JSON.parse(todos);                
                let todoIndex = todos.findIndex(todo => todo.id === this.state.id);
                if (todoIndex === -1) {
                    console.log('ADD TODO');
                    todos.push({id: new Date(), title: this.state.title , description: this.state.description});
                }  
                else {
                    console.log('EDIT TODO');
                    todos[todoIndex] = {id: new Date(), title: this.state.title , description: this.state.description};
                }
                AsyncStorage.setItem('todos', JSON.stringify(todos));
                console.log(todos);
                
            }            
            this.setState({title: '', description: ''})           
            
            //Go to home page
            //this.props.navigation.goBack();
            this.props.navigation.navigate('Home');
        });
    } catch (error) {
        alert(error);
    }
  }
}
export default EditScreen; 