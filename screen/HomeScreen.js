import React from 'react';
import stylesheet, { currentTheme } from '../Style'
import { AsyncStorage, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Badge, SearchBar, ListItem, Icon } from 'react-native-elements';

class HomeScreen extends React.Component {
    static navigationOptions = ({ navigation })=>{
      const { navigate } = navigation;
      const styles = stylesheet();
      return{
        title: 'Inicio', 
        headerRight: (
          <View style={styles.headerButton}>
            <Icon
              name='menu'
              style={{marginLeft: 20}}
              size={20}
              color="white"
              onPress={() => navigation.openDrawer()}
            />
          </View>
        )
      }
    };

    constructor(props) {
      super(props);
      this.state={
        searchText: '',
        todoList: []
      }      
    }

    componentWillMount() {
      this.navigationEvents = [
        this.props.navigation.addListener('didFocus', this.refreshTodoList)
      ];
    }

    componentWillUnmount() {
      this.navigationEvents.forEach(sub => sub.remove());
    }
     
    render() {
      const {navigate} = this.props.navigation;
      const styles = stylesheet();
      return (
        <View style={styles.container}> 
          <View style={styles.searchBarContainer}>
            <View style={styles.searchBar}>
              <SearchBar
                inputStyle={[styles.searchBarColor, {color: currentTheme.text}]}
                containerStyle={[styles.searchBarColor]}
                placeholderTextColor={currentTheme.text}
                onChangeText={ (searchTextValue) => {this.setState(() => ({ searchText : searchTextValue }))} }
                placeholder='Escribe el nombre de tu nota...' 
                value={ this.state.searchText }
              />
            </View>
            <View style={styles.badge}>
              <Badge
                value={this.state.todoList.length}
              />
            </View>
          </View>
          <View style={styles.todoList}>
            <ScrollView>
              {
                this.state.todoList.map((todo) => (this.filterTodos(todo)))
              }
            </ScrollView>
          </View>
          <View>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => {this.props.navigation.navigate('Edit', {id: 0, title:'', description:''})}}
                style={styles.touchableOpacityStyle}>
                <Image
                  source={require('../assets/Icons/add.png')}
                  style={styles.floatingButtonStyle}
                />
              </TouchableOpacity>
          </View>     
      </View>
      ); 
  }

  refreshTodoList = () => {
    AsyncStorage.getItem('todos').then((todos) => {
      if (todos !== null) {
        todos = JSON.parse(todos);
        this.setState({todoList: todos});
      }
    }); 
  }

  filterTodos = (todo) => {
    if (todo.title.toString().toLowerCase().indexOf(this.state.searchText.toString().toLowerCase()) > -1) {
      return (
          <ListItem
            key={todo.id}
            title={todo.title}
            titleStyle={{ color: currentTheme.text, fontWeight: 'bold' }}
            containerStyle={{backgroundColor: currentTheme.third, borderBottomColor: currentTheme.text}}
            chevronColor={currentTheme.text}
            onPress={() => {
              this.props.navigation.navigate('Detail', {id: todo.id, title: todo.title, description: todo.description})}
            }
          />
      )
    }
  }
}
export default HomeScreen;