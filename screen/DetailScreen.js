import React from 'react';
import stylesheet, { currentTheme } from '../Style'
import { View } from 'react-native';
import { Text, Icon, Card } from 'react-native-elements';
import { StackActions, NavigationActions } from 'react-navigation';

class DetailScreen extends React.Component {
    static navigationOptions = ({ navigation })=>{
        const { params = {} } = navigation.state;
        const { navigate } = navigation;
        const styles = stylesheet();
        return{
            title: 'Detalles nota',
            headerLeft: (
                <View style={styles.headerButton}>
                    <Icon
                    name='arrow-back'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => params._this.goHomeAction()}
                    />
                </View>
            ),
            headerRight: (
                <View style={styles.headerButton}>
                    <Icon
                    name='edit'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => navigate('Edit', {id: params._this.state.id, title: params._this.state.title, description: params._this.state.description})}
                    />
                </View>
            )
        }
    };

    constructor(props) {
        super(props);   
        this.state = {
            id: 0,
            title: '',
            description: ''
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ _this: this })
        this.setState({
            id: this.props.navigation.getParam('id'),
            title: this.props.navigation.getParam('title'), 
            description: this.props.navigation.getParam('description')
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        const styles = stylesheet();
        return (
            <View style={styles.container}>
                <Card 
                title={this.state.title}
                titleStyle={{ color: currentTheme.text, fontWeight: 'bold' }}
                containerStyle={{backgroundColor: currentTheme.third, borderColor: currentTheme.text}}
                dividerStyle={{backgroundColor: currentTheme.text}}>
                    <Text style={{ color: currentTheme.text, marginBottom: 10}}>{this.state.description}</Text>
                </Card>
            </View>
        );
    }

    goHomeAction = () => {
        const resetAction = StackActions.reset({
            key: null,
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        this.props.navigation.dispatch(resetAction);
    }
}
export default DetailScreen;