import React from 'react';
import stylesheet, { colors } from '../Style'
import { View } from 'react-native';
import { Button, Icon } from 'react-native-elements'

class SettingScreen extends React.Component {
    static navigationOptions = ({ navigation })=>{
        const { navigate } = navigation;
        const styles = stylesheet();
        return{
            title: 'Ajustes',
            headerLeft: (
                <View style={styles.headerButton}>
                    <Icon
                    name='arrow-back'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => navigation.navigate('Home')}
                    />
                </View>
            ),
            headerRight: (
                <View style={styles.headerButton}>
                    <Icon
                    name='menu'
                    style={{marginLeft: 20}}
                    size={20}
                    color="white"
                    onPress={() => navigation.openDrawer()}
                    />
                </View>
            )
        } 
    };

    constructor(props) {
        super(props);      
        this.state = {
            theme: 0
        }
    }
    
    render() {
        const {navigate} = this.props.navigation;
        let styles = stylesheet();
        return (
            <View style={styles.container}>
                <View style={styles.centeredContainer}>
                    <Button
                        title='Light'
                        backgroundColor={colors[0].first}
                        style={{flex: 1}}
                        onPress={() => {
                            stylesheet(0);
                            styles = stylesheet();
                            this.setState({theme:0});
                        }}
                    />
                    </View>
                <View style={styles.centeredContainer}>
                    <Button
                        title='Dark'
                        backgroundColor={colors[1].first}
                        style={{width: "100%"}}
                        onPress={() => {
                            stylesheet(1);
                            styles = stylesheet();
                            this.setState({theme:1});
                        }}
                    />
                </View>
                <View style={styles.centeredContainer}>
                    <Button
                        title='Alternative Blue'
                        backgroundColor={colors[2].first}
                        style={{flex: 1}}
                        onPress={() => {
                            stylesheet(2);
                            styles = stylesheet();
                            this.setState({theme:2});
                        }}
                    />
                </View>
            </View>
        );
    }
}
export default SettingScreen;