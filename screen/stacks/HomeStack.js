import { createDrawerNavigator, createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from "../HomeScreen";
import DeleteScreen from "../DeleteScreen";
import SettingScreen from "../SettingScreen";
import { currentTheme } from '../../Style';

const HomeScreenStack = createStackNavigator(
    {
        Home: { screen: HomeScreen } 
    },
    {
        initialRouteName: 'Home',
        headerMode: 'float',
        navigationOptions: { drawerLabel: 'Inicio' },
        defaultNavigationOptions: {
            headerStyle: {
              backgroundColor: currentTheme.header1,
            },
            headerTintColor: 'white',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
        } 
    }
);

const DeleteScreenStack = createStackNavigator(
    {
        Delete: { screen: DeleteScreen } 
    }, 
    {
        initialRouteName: 'Delete',
        headerMode: 'float',
        navigationOptions: { drawerLabel: 'Borrar notas' },
        defaultNavigationOptions: {
            headerStyle: {
              backgroundColor: currentTheme.header1,
            },
            headerTintColor: 'white',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
        } 
    }
);

const SettingsScreenStack = createStackNavigator(
    {
        Settings: { screen: SettingScreen } 
    }, 
    {
        initialRouteName: 'Settings',
        headerMode: 'float',        
        navigationOptions: { drawerLabel: 'Ajustes' },
        defaultNavigationOptions: {
            headerStyle: {
              backgroundColor: currentTheme.header1,
            },
            headerTintColor: 'white',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
        } 
    }
);

const HomeDrawerNavigator = createDrawerNavigator({
    Home: { screen: HomeScreenStack },
    Delete: { screen: DeleteScreenStack },
    Settings: { screen: SettingsScreenStack }
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    drawerPosition: 'right',
    drawerType: 'slide',
    drawerWidth: 140
  }); 
const HomeStack = createAppContainer(HomeDrawerNavigator);
export default HomeStack;