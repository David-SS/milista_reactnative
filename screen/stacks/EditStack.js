import { createStackNavigator, createAppContainer } from "react-navigation";
import EditScreen from "../EditScreen";
import { currentTheme } from "../../Style";

const EditStackCreator = createStackNavigator(
  {
    Edit: { screen: EditScreen }
  },
  {
    initialRouteName: 'Edit',
    headerMode: 'float',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: currentTheme.header2,
      },
      headerTintColor: 'white',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
);

const EditStack = createAppContainer(EditStackCreator);
export default EditStack;


