import { createStackNavigator, createAppContainer } from "react-navigation";
import DetailScreen from "../DetailScreen";
import { currentTheme } from "../../Style";

const DetailStackCreator = createStackNavigator(
  {
    Detail: { screen: DetailScreen }
  },
  {
    initialRouteName: 'Detail',
    headerMode: 'float',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: currentTheme.header2,
      },
      headerTintColor: 'white',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
);

const DetailStack = createAppContainer(DetailStackCreator);
export default DetailStack;


