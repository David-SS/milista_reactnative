import { StyleSheet } from 'react-native';

const container = { 
    flex: 1,  
    flexDirection: 'column',
    backgroundColor: '#000',
    //alignItems: 'center',
    //justifyContent: 'center',
}

const headerButton = {
    flex:1,
    padding:10
}

const searchBarContainer = {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#2D2D2D'
}

const badge = {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center'
}

const searchBar = {
    flex:8,
    flexDirection: 'column',
    justifyContent: 'center'
}

const todoList = {
    flex: 1,
    flexDirection: 'column',
}

const touchableOpacityStyle = {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
}

const floatingButtonStyle = {
    resizeMode: 'contain',
    width: 50,
    height: 50,
}

const textInput = {
    borderColor: 'gray',
    borderWidth: 1
}

// ========================================================
// APPLYING STYLES
// ========================================================

export const colors = [
    alternative = {
        first: '#607CE7',
        second: '#3B4DAF',
        third: '#47559C',
        fourth: '#9F589A',
        fifth: '#C36894'
    },

    dark = {
        first: '#130912',
        second: '#3E1C33',
        third: '#602749',
        fourth: '#B14623',
        fifth: '#F6921D'
    }, 

    light = {
        first: '#C9D1D3',
        second: '#F7F7F7',
        third: '#9DD3DF',
        fourth: '#3B3737',
        fifth: '#991818'
    }
]

var styleTheme = StyleSheet.create({
    container,
    headerButton,
    searchBarContainer,
    badge,
    searchBar,
    todoList,
    touchableOpacityStyle,
    textInput
});


export default function stylesheet() {
    return styleTheme;
}
