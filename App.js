import { createStackNavigator, createAppContainer } from "react-navigation";

import HomeStack from "./screen/stacks/HomeStack";
import DetailStack from "./screen/stacks/DetailStack";
import EditStack from "./screen/stacks/EditStack";


const RootStack = createStackNavigator({

  Home: { 
    screen: HomeStack,
    navigationOptions: { header: null } 
  },
  Detail: { screen: DetailStack },
  Edit: { screen: EditStack },
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

const App = createAppContainer(RootStack);
export default App;