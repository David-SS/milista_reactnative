import { StyleSheet } from 'react-native';
import { AsyncStorage } from 'react-native';

// ==============================================================
// ======================  COLORS  ==============================
// ==============================================================

export const colors = [
    light = {
        first: '#C9D1D3',
        second: '#F7F7F7',
        third: '#9DD3DF',
        fourth: '#4DA8E5',
        text: 'black',
        header1: '#2D2D2D',
        header2: '#2D2D2D'
    },
    dark = {
        first: '#130912',
        second: '#3E1C33',
        third: '#602749',
        fourth: '#4DA8E5',
        text: 'white',
        header1: '#2D2D2D',
        header2: '#2D2D2D'
    },
    alternativeBlue = {
        first: '#408CBF',
        second: '#316A91',
        third: '#56BAFF',
        fourth: '#4DA8E5',
        text: 'white',
        header1: '#2D2D2D',
        header2: '#2D2D2D'        
        //header1: '#62c8ff',
        //header2: '#ae6aff'
    },
]

// Get last style from asyncstorage...
export var currentTheme = colors[0];


// ==============================================================
// ======================  STYLES  ==============================
// ==============================================================

getStyles = () => {
    return ({
        container: {
            flex: 1,  
            flexDirection: 'column',
            backgroundColor: currentTheme.first,
        },

        centeredContainer: {
            flex:1,
            alignItems: 'center',
            justifyContent: 'center',
        },

        headerButton: {
            flex:1,
            padding:10
        },

        searchBarContainer: {
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: currentTheme.second,
        },

        searchBar: {
            flex:8,
            flexDirection: 'column',
            justifyContent: 'center',
        },

        searchBarColor: {
            backgroundColor: currentTheme.second,
            borderBottomColor: 'transparent',
            borderTopColor: 'transparent'
        },

        badge: {
            flex:1,
            flexDirection: 'column',
            justifyContent: 'center'
        },

        todoList: {
            flex: 1,
            flexDirection: 'column',
        },

        touchableOpacityStyle: {
            position: 'absolute',
            width: 50,
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            right: 30,
            bottom: 30,
        },

        floatingButtonStyle: {
            resizeMode: 'contain',
            width: 50,
            height: 50,
        },

        editContainer: {
            padding: 10
        },

        textInput: {
            textAlignVertical: 'top',
            color: currentTheme.text,
            //borderColor: currentTheme.text,
            borderWidth: 0,
            padding: 10
        }        

    });
}

// ==============================================================
// ======================  APPLYING =============================
// ==============================================================

var styleTheme = () => {
    return (StyleSheet.create(getStyles()));
}

export default function stylesheet(code) {
    if (code !== undefined) {
        currentTheme = colors[code];
    }
    return styleTheme();
}